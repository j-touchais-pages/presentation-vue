import { defineShikiSetup } from "@slidev/types/dist/index.mjs";

export default defineShikiSetup(async () => {
  return {
    theme: {
      dark: "github-dark",
      light: "github-dark",
    },
  };
});
