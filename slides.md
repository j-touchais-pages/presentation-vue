---
theme: academic
highlighter: shiki
lineNumbers: false
info: |
  ## Présentation Vue.js
  Présentation de Vue.js pour l'AG genotoul.

  Date: 07/12/2023=
transition: slide-up
mdc: true
coverAuthor: Julien Touchais
coverAuthorUrl: https://forgemia.inra.fr/julien.touchais
title: Vue.js
---

<!-- prettier-ignore-start -->

#

Comment en mettre plein les yeux avec

<h1 class="font-bold relative text-primary"><logos-vue /> Vue.js<span class="blur-md absolute left-0 top-0"><logos-vue /> Vue.js</span></h1>

<!--
Bonjour à tous,

Je m'appelle Julien Touchais et je suis développeur web sur la plateforme bioinfo.

On a **régulièrement besoin** d'avoir un **site web** pour **présenter tel ou tel projet** ou bien mettre une **base de données** à disposition.

Pour faire ça, la **solution choisie** c'est bien souvent de s'orienter vers des **outils comme WordPress** (_comme le site de genotoul_) ou Drupal qui permettent de **créer et administrer rapidement et facilement** un site.

Cependant, quand on a besoin de **fonctionnalités plus avancée** que la simple **présentation de contenu**, et bien **ces solutions** elles vont montrer leur **limites**.

C'est donc **à ce moment-là** qu'interviennent des **outils comme celui** dont je vais vous **parler aujourd'hui** et grâce auquel vous pourrez créer des sites qui en mettent plein les yeux: **Vue.js** (je sais, c'est écrit en français mais ça se dit à l'anglaise, allez savoir)
-->

---
src: ./pages/historique.md
---

---
src: ./pages/vue.md
---

---
src: ./pages/conclusion-avec-exemple.md
---

---
layout: end
class: gap-4 !text-start 
title: Fin & refs
---

<h6 class="!opacity-80 !font-bold">Icônes : logos, mdi, simple-icons, emojione, skill-icons, noto, noto-v1, uiw, fluent, remix</h6>
<h6 class="!opacity-80 !font-bold">GIFs: Tenor (diapo 6)</h6>

 <p class="absolute bottom-0 left-1/2 -translate-x-1/2 text-xs flex flex-col items-center" >
 <a href="https://forgemia.inra.fr/jt/presentation-vue" target="_blank" class="hover:text-[#FCA326]!"><ph-gitlab-logo-duotone /> forgemia.inra.fr/jt/presentation-vue</a>
 <a xmlns:cc="http://creativecommons.org/ns#" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="license noopener noreferrer">CC BY-SA 4.0<ri-creative-commons-fill class="ml-1 -mb-.25"/><ri-creative-commons-by-fill class="ml-1 -mb-.25"/><ri-creative-commons-sa-fill class="ml-1 -mb-.25"/></a></p> 

<!-- prettier-ignore-end -->
