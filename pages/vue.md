---
layout: two-cols-header
clicks: 4
transition: slide-left
title: "Vue: concept"
---

# <span class="relative"><logos-vue class="blur-sm -z-1" absolute top-0 left-0 /><logos-vue class=""/></span> Pourquoi <span class="text-primary">**Vue.js**</span> ?

::left::

<div class="text-center">

  <h2 v-click class="mb-4 duration-500">Concept de base :</h2>

<h3 v-after class="delay-200 duration-500">Diviser son site en <span class="text-primary "><strong>composants</strong> <uiw-component/></span></h3>

  <div v-after class="delay-400 duration-500 mt-8 grid grid-cols-3 grid-rows-3 items-center gap-x-4">
  
  <div class="border-dashed border-2 row-span-2 border-slate-500 rounded-xl h-full flex items-center justify-center p-2">
    <Button color='var(--primary-color)' primary class="text-sm" />
  </div>

  <div class="border-dashed border-2 row-span-2 border-slate-500 rounded-xl h-full flex items-center justify-center p-2">
    
  ![Graph representation](/images/2-vue/1-graph.png)

  </div>

  <div class="border-dashed border-2 row-span-2 border-slate-500 rounded-xl h-full flex items-center justify-center p-2">
    
  ![Site page](/images/2-vue/2-page.png)

  </div>

  <div class="col-span-3 mt-8">

#### Pour pouvoir les <span class="text-primary">**réutiliser**</span> et les <span class="text-primary">**partager**</span> !

  </div>

  </div>

</div>

<!-- <div class="absolute -top-24 left-16 -right-36"> -->

<v-click>

<svg class="absolute -top-36 left-16 w-128" width="708.329" height="498.492" viewBox="0 0 187.412 131.893" xmlns="http://www.w3.org/2000/svg" stroke="var(--primary-color)"><path d="M.53 108.44c.386 7.138 4.241 14.01 10.133 18.06 3.82 2.626 8.372 4.071 12.977 4.605 4.605.534 9.276.186 13.853-.548 9.155-1.468 18.066-4.476 27.315-5.131 11.107-.787 22.128 1.848 33.157 3.381 5.515.767 11.093 1.26 16.65.911 5.556-.35 11.11-1.565 16.08-4.073 5.724-2.889 10.519-7.42 14.287-12.607 3.768-5.187 6.554-11.028 8.865-17.008 4.622-11.961 7.457-24.676 13.574-35.946 1.107-2.04 2.346-4.058 4.092-5.586 1.246-1.092 2.738-1.91 4.34-2.33 1.604-.419 3.317-.433 4.915.005 1.598.437 3.075 1.331 4.169 2.576 1.093 1.245 1.794 2.839 1.924 4.49.186 2.368-.835 4.781-2.61 6.36-1.773 1.578-4.247 2.311-6.606 2.041-2.17-.248-4.19-1.295-5.875-2.683-1.686-1.388-3.065-3.108-4.35-4.873-5.96-8.192-10.212-17.994-10.051-28.124.08-5.066 1.283-10.14 3.721-14.581 2.438-4.44 6.135-8.223 10.645-10.532 3.937-2.015 7.986-2.897 12.39-2.48-2.561 2.31-4.133 3.514-5.502 4.623 1.538-.744 5.255-2.185 7.64-4.526-.998-1.482-3.428-2.884-5.643-3.935" fill="none" stroke-width="1.058" stroke-linecap="round" stroke-linejoin="round"/></svg>

</v-click>

<!-- </div> -->

::right::

<div class="absolute -top-2/5 left-2/5 scale-65 origin-top-left">

<v-after>

<div class="sfc-code delay-1400 duration-200 shadow-black shadow-xl rounded">

```vue
  <template>
    <button
      @click="$emit('click')"
      :style="
        primary
          ? {
              backgroundColor: color,
              color: 'white',
              borderBottom: '2px darkgreen solid',
            }
          : {
              backgroundColor: 'white',
              border: `2px ${color} solid`,
              color: color,
            }
      "
    >
      <slot name="content">Clique-moi !</slot>
    </button>
  </template>
  
  <style scoped>
  button {
    font-weight: bold;
    border-radius: 0.5rem;
    padding: 0.25rem 0.5rem;
    margin: auto 0;
  }
  </style>

  <script setup lang="ts">
  defineProps<{
    color: string;
    primary: boolean;
  }>();
  
  defineEmits<{
    click: [];
  }>();
  </script>

```

</div>

<div class="absolute delay-1400 -rotate-90 bottom-0 text-2xl font-bold text-primary drop-shadow-[0_0_.7rem_currentColor] origin-bottom-left -left-4">Tout en un !</div>

</v-after>
  
<div class="bg-white opacity-80 absolute top-8 left-2 right-2 h-81 rounded-lg flex flex-col items-center justify-center text-3xl font-bold shadow-lg shadow-white duration-300 gap-2" v-click="3">
<span color-black block>HTML <simple-icons-html5 class="text-orange-6 ml-2" /></span><span color-black block v-click="4" ml-20>&rdca; Contenu <noto-v1-hammer/></span>
</div>
  
<div class="bg-white opacity-80 absolute top-103 left-2 right-2 h-26 rounded-lg flex items-center justify-center text-3xl font-bold shadow-lg shadow-white delay-300 duration-300 flex-col gap-2" v-click="3">
<span color-black block>CSS <simple-icons-css3 class="text-blue ml-2" /></span><span color-black block v-click="4" ml-14 delay-300 duration-300>&rdca; Style <noto-v1-artist-palette/></span>
</div>
  
<div class="bg-white opacity-80 absolute top-143 left-2 right-2 h-36 rounded-lg flex flex-col items-center justify-center text-3xl font-bold shadow-lg shadow-white delay-600 duration-600 gap-2"  v-click="3">
<span color-black block>JavaScript <logos-javascript class="ml-2" /></span><span color-black block v-click="4" ml-10 delay-600 duration-300>&rdca; Interaction <noto-v1-gear/></span>
</div>

</div>

<!--
Le **principe**, avec Vue, ça va être de **diviser son code** (puisqu'on écrit quand même du code hein) en **composants**, qui sont des blocs qu'on pourra **réutiliser** par la suite dans son site : ça peut aller du **bouton** à l'affichage de **graphe**, même une **page complète** c'est en fait un composant.

Pour décrire un composant, on va utiliser un **seul fichier** séparé en **trois parties**, contrairement à la manière habituelle de faire où on doit jongler entre les différents types de fichiers, qui correspondent à notre triptyque **HTML / CSS / JavaScript**, autrement dit : **contenu, style et interaction**.
-->

---
transition: slide-left
title: "Vue: différences"
---

# <span class="relative"><logos-vue class="blur-sm -z-1" absolute top-0 left-0 /><logos-vue class=""/></span> Pourquoi <span class="text-primary">**Vue.js**</span> ?

<div class="flex flex-col gap-8 items-center mt-20">

<span class="mb-5">

## Particularités

</span>

<v-clicks>

  ###### Entièrement <span class="text-primary font-bold drop-shadow-[0_0_.5rem_currentColor]">communautaire <fluent-people-community-16-regular class="text-xl align-middle"/></span>, libre comme l'air

  ###### <span class="text-primary font-bold drop-shadow-[0_0_.5rem_currentColor]">Rapide <fluent-top-speed-20-filled class="text-xl align-middle"/></span> comme l'éclair

  ###### Progressif et <span class="text-primary font-bold drop-shadow-[0_0_.5rem_currentColor]">modulaire <fluent-puzzle-cube-piece-20-regular class="text-xl align-middle"/></span>

</v-clicks>
  
</div>

<!--
Alors évidemment, Vue ce n'est **pas le seul** à avoir eu cette **bonne idée** des **composants**, il existe **d'autres _frameworks_** (peut-être que les noms de React ou d'Angular parlent à certains d'entre vous), mais il arrive à se **différencier** par certains points :

- Premier et non des moindre : il est **100% communautaire**, ce qui fait qu'il ne prendra **pas d'orientation** qui pourraient servir des **intérêts autres que ceux des utilisateurs**.
- Ensuite, il est **rapide** sur plein de tableaux : rapide **à utiliser** pour le **développeur**, **rapide à apprendre** (la courbe d'apprentissage est une des meilleures), et encore **rapide à l'exécution** puisqu'il est assez **léger** et **performant**.
- Enfin, il est **modulaire**, ou _progressif_ selon leurs termes, puisqu'il est doté d'un **écosystème très riche** avec **pleins d'outils** qui s'intègrent parfaitement avec et **qu'on peut ajouter au fur et à mesure**. Donc on peut **commencer très simple** et **enrichir progressivement** jusqu'à avoir le site du futur.
-->

---
title: "Vue: plus loin"
---

# <span class="relative"><logos-vue class="blur-sm -z-1" absolute top-0 left-0 /><logos-vue class=""/></span> Avec <span class="text-primary">**Vue.js**</span>

<div class="flex flex-col gap-16 items-center mt-30">

<v-clicks>

  <h6 class="relative">
  De la <span class="text-primary font-bold drop-shadow-[0_0_0.5rem_currentColor]">documentation</span> facile avec <span class="text-primary font-bold drop-shadow-[0_0_0.5rem_currentColor]">Vitepress</span>
  <img alt="Vitepress" src="/images/2-vue/4-vitepress.png" class="absolute h-32 rotate-20 -right-36 -top-20" />
  </h6>

  <h6 class="relative">
  Des <span class="text-primary font-bold drop-shadow-[0_0_0.5rem_currentColor]">applications mobiles</span> ou de <span class="text-primary font-bold drop-shadow-[0_0_0.5rem_currentColor]">bureau</span>

  <img alt="SnoBoard PWA" src="/images/2-vue/5-pwa.png" class="absolute h-40 -rotate-10 -left-38 -top-16" />
  <div class="h-10 w-22 absolute -left-50 -top-20 bg-white shadow-[0_0_1rem_white] bg-opacity-90"></div>
  <logos-pwa class="absolute  -left-48 -top-24 text-6xl"/>
  <img alt="SnoBoard Electron" src="/images/2-vue/6-electron.png" class="absolute h-40 rotate-10 -right-72 top-0" />
  <skill-icons-electron class="absolute -right-68 top-28 text-5xl"/>
  </h6>

  <h6> Et <span class="text-primary font-bold drop-shadow-[0_0_0.5rem_currentColor]">plus encore...</span> <span class="text-[0.5rem]">Oui oui, ce diapo c'est encore Vue</span></h6>
  
  <fluent-emoji-eyes class="text-6xl absolute left-1/2 bottom-12 -translate-x-1/2" />

</v-clicks>

</div>


<!--
Alors bien sûr avec Vue, on peut donc construire des sites web, mais ça ne s'arrête pas là ! Parce que la beauté de Vue permet aussi de faire  :

- De la **documentation** facilement avec des outils comme Vitepress qui s'occupent de tout
- De porter nos applications vers le mobile ou le bureau grâce aux **Progressive Web Apps** et **Electron**
- Et même des présentations puisque celle que vous avez sous vos yeux a été faite avec un outil qui fonctionne sur Vue

Donc vraiment **les usages sont très variés**, alors **n'hésitez pas** à aller jeter un **petit coup d'oeil** à Vue !
-->