---
layout: center
class: text-center content-start
title: "Exemple : snoBoard & Conclusion"
clicks: 1
---

<div v-click="1" >
  <h1 v-click="[0,1]" class="example-title transition-all duration-1500 !text-xl mt-10">
  L'application sur laquelle je travaille
  </h1>

  <h2 v-after class="example-snoboard relative !text-3xl font-bold text-primary !transition-all duration-1500 after:absolute after:top-3 after:left-0 after:right-2 after:h-full after:content-empty inline">
  snoBoard
<LightOrDark>
<template #light>
  <span class="animation-mask absolute top-10 right-0 w-0 h-5 z-1 bg-white transition-width duration-1500"></span>
</template>
<template #dark>
  <span class="animation-mask absolute top-10 right-0 w-0 h-5 z-1 bg-[var(--slidev-slide-container-background,black)] transition-width duration-1500"></span>
</template>
</LightOrDark>
  </h2>
</div>

<img v-click="1" src="/images/3-snoboard/1-table.png" alt="SnoBoard vue 7" height="764" width="1490" class="duration-700 delay-1000 scale-30 origin-top-left absolute top-8 left-10 rounded-3xl"/>
<img  v-click="1" src="/images/3-snoboard/2-interaction.png" alt="SnoBoard vue 3" height="677" width="1574" class="duration-700 delay-1200 scale-30 origin-top-left absolute top-51 left-5 rounded-3xl"/>
<img  v-click="1" src="/images/3-snoboard/3-struct.png" alt="SnoBoard vue 6" height="755" width="1600" class="duration-700 delay-1400 scale-30 origin-top-left absolute top-90 left-8 rounded-3xl"/>
<img  v-click="1" src="/images/3-snoboard/4-guide.png" alt="SnoBoard vue 2" height="550" width="1172" class="duration-700 delay-1600 scale-30 origin-top-left absolute top-95 left-86 rounded-3xl"/>
<img  v-click="1" src="/images/3-snoboard/5-chromosome.png" alt="SnoBoard vue 1" height="874" width="1600" class="duration-700 delay-1800 scale-30 origin-top-left absolute top-90 left-163 rounded-3xl"/>
<img  v-click="1" src="/images/3-snoboard/6-karyotype.png" alt="SnoBoard vue 4" height="535" width="1600" class="duration-700 delay-2000 scale-30 origin-top-left absolute top-55 left-165 rounded-3xl"/>
<img  v-click="1" src="/images/3-snoboard/7-sequence.png" alt="SnoBoard vue 5" height="942" width="1600" class="duration-700 delay-2200 scale-30 origin-top-left absolute top-5 left-160 rounded-3xl"/>

<h1 class="text-primary drop-shadow-[0_0_1rem_currentColor] font-bold absolute top-1/2 left-1/2 translate-[-50%] !text-6xl block">Merci !</h1>

<!--
**Merci** pour votre attention, désolé pour les **mauvais jeux de mots**, et voici un **petit aperçu** de ce qu'on peut faire avec Vue
-->
